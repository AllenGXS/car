# gps车辆定位管理系统

#### 介绍
通过给车辆安装gps定位器，实现车辆的监控管理

#### 软件架构
软件架构说明
主要框架
1.python：
2.django 
3.twisted
4.数据库：postgresql，最新更改为sqlite，减少部署发麻烦
5.前端：layui
6.中间件：redis


#### 安装和使用教程

1. python manage.py createsuperuser, 用户名为：admin,密码随意设置
1. python manage.py runserver 有缺少的库，参照requirements.txt，网页输入127.0.0.1:8000可以看到运行结果
1. python datahandle/receive_serv.py，独立运行,功能：数据接收，并解析，解析出来时候放入redis，设备的监听是10808，默认协议是jt808部标通讯协议，目前也只解析了定位和一些基本参数。
1. python datahandle/store_serv.py，独立运行,功能：从redis接受解析好的数据，进行状态计算（超速报警，围栏报警等）和存储

1. 可以自己登录http://114.67.122.37:10889

入行时候的作品，比较粗糙，甚至愚蠢，不喜勿喷。
微调之后，可以直接用于小型车队的调度管理。

最后放些效果图
![输入图片说明](car1.jpg)
![输入图片说明](car2.jpg)
![输入图片说明](car_apply.png)
![输入图片说明](car3.png)
![输入图片说明](move_line.png)




