# -*- coding: utf-8 -*-
# @Author  : 吕康宁
# @Time    : 2019/12/24 16:08
# @File    : gumi_command.py
import binascii


# 断油电，参数0表示执行回复，1表示执行断开，常规，多个参数用逗号隔开，例如1,2,3,字符串的形式发送
def gumi_command(device_id,kind,param,model):
	command='6767'

	title_dict={'start_sound_record':'0A','close_sound_record':'0B','push_to_device':'81'}

	command_80_dict={'call_now':'WHERE','query_param':'PARAM','query_verson':'VERSION','query_status':'STATUS','factory':'FACTORY','dev_reset':'RESET',
					  'query_time_zone':'GMT','query_center_num':'CENTER','query_sos':'SOS','query_sos_alarm':'SOSALM','query_remove_alarm':'SWT',
					  'query_low_power_alarm':'BATALM','query_poweroff_set':'POWERALM','query_sensor_set':'SENALM','query_gpson':'GPSON',
					  'query_defence':'DEFENSE','query_static_speed_drop':'SF','query_gprs_set':'GPRSSET'}

	command_80_with_param={'cut_oil':'RELAY','send_rate':'TIMER','wakeup':'WAKEUP','set_time_zone':'GMT','language':'LANG','center_num_set':'CENTER',
						   'sos':'SOS','sos_set':'SOSALM','dev_remove_alarm':'SWT','low_power_alarm':'BATALM','poweroff_set':'POWERALM','sensor_set':'SENALM',
						   'gpson':'GPSON','defence':'DEFENSE','sensitivity':'LEVER','static_speed_drop':'SF'
						   }

	command_id_dict={'query_param':'00000001','call_now':'00000002','query_verson':'00000003','query_status':'00000004','factory':'00000005','dev_reset':'00000006',
					 'cut_oil':'00000007','send_rate':'00000008','wakeup':'00000009','set_time_zone':'00000010','query_time_zone':'00000011',
					 'language':'00000012','center_num_set':'00000013','query_center_num':'00000014','sos':'00000015','query_sos':'00000016',
					 'query_sos_alarm':'00000017','sos_set':'00000018','dev_remove_alarm':'00000019','query_remove_alarm':'00000020',
					 'low_power_alarm':'00000021','query_low_power_alarm':'00000022','query_poweroff_set':'00000023','poweroff_set':'00000024',
					 'query_sensor_set':'00000025','sensor_set':'00000026','query_gpson':'00000027','gpson':'00000028','query_defence':'00000029',
					 'defence':'00000030','sensitivity':'00000031','query_static_speed_drop':'00000032','static_speed_drop':'00000033','query_gprs_set':'00000034'}
	data_len=''

	if kind in title_dict:
		command=command+title_dict[kind]+'0002'+'0001'
		command=bytes.fromhex(command)

	elif kind in command_80_dict:
		data_len=len(command_80_dict[kind])+2+5+1
		data_len='%04x' % data_len
		command_value=(command_80_dict[kind]+'#').encode()
		command_value=bytes.hex(command_value)
		serv_id=command_id_dict[kind]
		command=command+'80'+data_len+'0001'+'01'+serv_id+command_value
		command=bytes.fromhex(command)
	elif kind in command_80_with_param:
		data_len=len(command_80_with_param[kind])+2+5+1+1+len(param)
		data_len='%04x' % data_len
		command_value=(command_80_with_param[kind]+','+param+'#').encode()
		command_value=bytes.hex(command_value)
		serv_id=command_id_dict[kind]
		command=command+'80'+data_len+'0001'+'01'+serv_id+command_value
		command=bytes.fromhex(command)

	return command

